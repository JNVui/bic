package com.example.jjacquot.bic.interfaces

/**
 * Created by jips on 10/18/17.
 */
interface IBIC {

    /** Connect to the BIC*/
    fun connect()

    /** Disconnect*/
    fun disconnect()

    /**Check the connection*/
    fun isConnect(): Boolean

    /**Read data from BIC each read return one line from the reading buffer*/
    fun read(): String

    /** Write data in BIC*/
    fun write(data: String)

}