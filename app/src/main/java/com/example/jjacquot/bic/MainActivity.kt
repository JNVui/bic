package com.example.jjacquot.bic

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import com.example.jjacquot.bic.interfaces.DataNotify
import com.example.jjacquot.bic.interfaces.IUARTInterpretor
import com.hoho.android.usbserial.driver.UsbSerialPort
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), DataNotify{


    lateinit var mInterfaceUart: IUARTInterpretor
    lateinit var mPathToDirectory: String
    lateinit var mDataReceived: String

    val mScreenRefreshHandler: Handler = Handler()

    val mScreenRefreshRunnable = object : Runnable {
        override fun run() {
            try {
                textView5.text = mInterfaceUart.getUARTOutput(mActualUART)
                scrollViewUART.fullScroll(ScrollView.FOCUS_DOWN)

                if(mActualUART == "DI") {
                    when (mDataReceived) {
                        "DI1R1" -> checkboxdi1.isChecked = true
                        "DI1R0" -> checkboxdi1.isChecked = false

                        "DI2R1" -> checkboxdi2.isChecked = true
                        "DI2R0" -> checkboxdi2.isChecked = false

                        "DI3R1" -> checkboxdi3.isChecked = true
                        "DI3R0" -> checkboxdi3.isChecked = false

                        "DI4R1" -> checkboxdi4.isChecked = true
                        "DI4R0" -> checkboxdi4.isChecked = false

                        "DI5R1" -> checkboxdi5.isChecked = true
                        "DI5R0" -> checkboxdi5.isChecked = false

                        "DI6R1" -> checkboxdi6.isChecked = true
                        "DI6R0" -> checkboxdi6.isChecked = false

                        "DI7R1" -> checkboxdi7.isChecked = true
                        "DI7R0" -> checkboxdi7.isChecked = false

                        "DI8R1" -> checkboxdi8.isChecked = true
                        "DI8R0" -> checkboxdi8.isChecked = false
                    }
                }
            } catch (e : Exception){
                textView5.text = e.message + e.stackTrace
            }
        }
    }

    override fun dataNotify(data : String) {
        mDataReceived = data
        mScreenRefreshHandler.postDelayed(mScreenRefreshRunnable, 100) //start screen auto refresh
    }

    fun switchDIMode() {
            scrollViewDI.visibility = View.VISIBLE
            connect_button.visibility = View.INVISIBLE
            deconnect_button.visibility = View.GONE
            baud_list_spinner.visibility = View.GONE
            command_editText.visibility = View.GONE
            send_button.visibility = View.GONE
            clear_button.visibility = View.GONE
    }

    fun switchUARTMode() {
            scrollViewDI.visibility = View.GONE
            connect_button.visibility = View.VISIBLE
            deconnect_button.visibility = View.VISIBLE
            baud_list_spinner.visibility = View.VISIBLE
            command_editText.visibility = View.VISIBLE
            send_button.visibility = View.VISIBLE
            clear_button.visibility = View.VISIBLE
    }

    fun switchBICMode() {
            scrollViewDI.visibility = View.GONE
            connect_button.visibility = View.INVISIBLE
            deconnect_button.visibility = View.GONE
            baud_list_spinner.visibility = View.GONE
            command_editText.visibility = View.VISIBLE
            send_button.visibility = View.VISIBLE
            clear_button.visibility = View.VISIBLE
    }


    var mActualUART: String = ""

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        mPathToDirectory = cacheDir.absolutePath
        mInterfaceUart = mockUART(cacheDir.absolutePath, MockBIC())
        mInterfaceUart.setDataListenner(this)
        mInterfaceUart.connect(this, mPort)

        //On recupere la List des UART
        //Om ajoute au Spinner
        val list: List<String> = mInterfaceUart.getUARTList()
        uart_list_spinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list) as SpinnerAdapter?
        uart_list_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                textView5.text = "Please Select an Option"
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                mActualUART = mInterfaceUart.getUARTList().get(position)
                when(mActualUART){
                    "DI" -> {switchDIMode()}
                    "BIC" -> {switchBICMode()}
                    "UART1" -> {switchUARTMode()}
                    "UART2" -> {switchUARTMode()}
                    "UART3" -> {switchUARTMode()}
                    "UART4" -> {switchUARTMode()}
                }
                dataNotify(mActualUART)
            }
        }

        //On recupere la List des Baud
        //Om ajoute au Spinner
        baud_list_spinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mInterfaceUart.getBaudRateList())

        //connection
        connect_button.setOnClickListener {
            mInterfaceUart.write(mActualUART, CommandList.getStartUARTCommand(mActualUART, baud_list_spinner.selectedItem.toString()))

        }

        deconnect_button.setOnClickListener {
            mInterfaceUart.write(mActualUART, CommandList.getStopUARTCommand(mActualUART, baud_list_spinner.selectedItem.toString()))
        }

        //send data
        send_button.setOnClickListener {
            try {
                if(mActualUART == "BIC") {
                    mInterfaceUart.write(mActualUART, command_editText.text.toString())
                }else{
                    mInterfaceUart.write(mActualUART, CommandList.getSendDataToUARTCommand(mActualUART, command_editText.text.toString()))
                }

            } catch (e: Exception) {
                textView5.text = e.message + "\n" + e.cause
            }
            command_editText.setText("")

        }

        //clear
        clear_button.setOnClickListener {
            mInterfaceUart.clear(mActualUART)
        }

        //DI
        assignDI(checkboxdi1, statusDiTextview1, rebonceDIEditText1, samplingDUEditText1, eventDUCheckBox1, sendDI1, refreshDI1)
        assignDI(checkboxdi2, statusDiTextview2, rebonceDIEditText2, samplingDUEditText2, eventDUCheckBox2, sendDI2, refreshDI2)
        assignDI(checkboxdi3, statusDiTextview3, rebonceDIEditText3, samplingDUEditText3, eventDUCheckBox3, sendDI3, refreshDI3)
        assignDI(checkboxdi4, statusDiTextview4, rebonceDIEditText4, samplingDUEditText4, eventDUCheckBox4, sendDI4, refreshDI4)
        assignDI(checkboxdi5, statusDiTextview5, rebonceDIEditText5, samplingDUEditText5, eventDUCheckBox5, sendDI5, refreshDI5)
        assignDI(checkboxdi6, statusDiTextview6, rebonceDIEditText6, samplingDUEditText6, eventDUCheckBox6, sendDI6, refreshDI6)
        assignDI(checkboxdi7, statusDiTextview7, rebonceDIEditText7, samplingDUEditText7, eventDUCheckBox7, sendDI7, refreshDI7)
        assignDI(checkboxdi8, statusDiTextview8, rebonceDIEditText8, samplingDUEditText8, eventDUCheckBox8, sendDI8, refreshDI8)

    }

    fun assignDI(nameCheckBox: CheckBox, statusTextView: TextView, rebonceEditText: EditText, sampling: EditText, eventCheckBox: CheckBox, sendButton: Button, refreshButton: Button) {

        var diName = nameCheckBox.text.toString()
        var rebonceValue: String
        var samplingValue: String

        nameCheckBox.isClickable = false
        sendButton.setOnClickListener {

            if (rebonceEditText.text.toString().toIntOrNull() == null) {
                rebonceValue = "-"
            } else {
                rebonceValue = rebonceEditText.text.toString()
            }


            if (sampling.text.toString().toIntOrNull() == null) {
                samplingValue = "-"
            } else {
                samplingValue = sampling.text.toString()
            }

            try {
                mInterfaceUart.write(diName, CommandList.getSetDICommand(diName, rebonceValue, samplingValue, eventCheckBox.isChecked))
            } catch (e : Exception){
                textView5.text = e.message + e.stackTrace
            }


        }

        refreshButton.setOnClickListener {
            try {
            mInterfaceUart.write(diName, CommandList.getDIStatusCommand(diName))
            } catch (e : Exception){
                textView5.text = e.message + e.stackTrace
            }
        }


    }

    override fun onResume() {
        super.onResume()
        //

    }

    companion object {
        lateinit var mPort: UsbSerialPort

        @JvmStatic
        fun show(context: Context, port: UsbSerialPort) {
            mPort = port
            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_NO_HISTORY)
            context.startActivity(intent)
        }
    }

}
