package com.example.jjacquot.bic

/**
 * Created by jjacquot on 25/10/17.
 */


class CommandList {

    companion object {
        val bic: String = "BIC"
        val error: String = "error"

        //Dictionary link command to uart file
        val uartFileHasmap: HashMap<String, String> =
                hashMapOf("S1" to "UART1", "S2" to "UART2", "S3" to "UART3", "S4" to "UART4", "DI" to "DI", "DO" to "DO")

        fun getFileFromCommandLine(command: String): String {
            if (command.length < 2){
                return error
            }

            var commandInt = command.substring(0, 2) //get two letters
            var uartFile: String? = uartFileHasmap.get(commandInt)
            return if (uartFile != null) {
                uartFile
            } else {
                error
            }
        }

        fun getListFile(): List<String> {
            return arrayListOf("UART1", "UART2", "UART3", "UART4", "DI", "BIC")
        }


        fun getStartUARTCommand(uartNum: String, baudrate: String): String {
            //ex:S1O9600

            //Get number of the UART
            var command = "S" + uartNum.elementAt(uartNum.lastIndex) + "O" + baudrate

            return command
        }

        fun getStopUARTCommand(uartNum: String, baudrate: String): String {
            //ex:S1O9600

            //Get number of the UART
            var command = "S" + uartNum.elementAt(uartNum.lastIndex) + "T"

            return command
        }


        fun getSendDataToUARTCommand(uartNum: String, data: String): String {
            //ex:S1O9600

            //Get number of the UART
            var command = "S" + uartNum.elementAt(uartNum.lastIndex) + "T" + data

            return command
        }

        fun getDIStatusCommand(diName: String): String {

            var command = "DI" + diName.elementAt(diName.lastIndex) + "R"
            return command
        }

        fun getSetDICommand(diName: String, timeAntiRebonceInMillis: String, timeSamplingInMillis: String, activeEvent: Boolean): String {
            //DInDxxxPyyyE
            var command: String
            if (activeEvent) {
                command = "DI" + diName.elementAt(diName.lastIndex) + "D" + timeAntiRebonceInMillis + "P" + timeSamplingInMillis + "E"
            } else {
                command = "DI" + diName.elementAt(diName.lastIndex) + "D" + timeAntiRebonceInMillis + "P" + timeSamplingInMillis
            }

            return command
        }

    }


}