package com.example.jjacquot.bic

import android.content.Context
import android.hardware.usb.UsbManager
import android.icu.text.DateFormat
import android.icu.util.Calendar
import android.os.SystemClock
import com.example.jjacquot.bic.CommandList.Companion.getFileFromCommandLine
import com.example.jjacquot.bic.interfaces.DataNotify
import com.example.jjacquot.bic.interfaces.IBIC
import com.example.jjacquot.bic.interfaces.IUARTInterpretor
import com.hoho.android.usbserial.driver.UsbSerialPort
import com.hoho.android.usbserial.util.SerialInputOutputManager
import java.io.IOException
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors


/**
 * Created by jjacquot on 17/10/17.
 */
class mockUART : IUARTInterpretor {

    lateinit var mPath: String
    lateinit var mBic: IBIC
    lateinit var mPort: UsbSerialPort
    var mDataAvailable: Boolean = false
    private val mExecutor = Executors.newSingleThreadExecutor()
    lateinit var mDataListenner : DataNotify

    constructor(path: String, bic: IBIC) {
        mPath = path
        mBic = bic
    }

    lateinit var mSerialIoManager: SerialInputOutputManager

    private val mListener = object : SerialInputOutputManager.Listener {

        override fun onRunError(e: Exception) {
        }

        override fun onNewData(data: ByteArray) {
            var s: String
            s = data.toString(Charset.defaultCharset())
            for (command in s.split("\n")) {

                var uartFile = getFileFromCommandLine(command)
                if (uartFile == CommandList.error){
                    continue
                }
                var timestamp = System.currentTimeMillis()

                var formatter = SimpleDateFormat("hh:mm:ss:SSS")
                var dateString = formatter.format(Date(timestamp))


                FileManager.writeFile(mPath, uartFile, "[" + dateString + "] : " + command  + "\n")
                FileManager.writeFile(mPath, CommandList.bic, "[" + dateString + "] : " + data.contentToString() + command  + "\n")
                mDataListenner.dataNotify(command)
            }

        }
    }

    private fun stopIoManager() {
        mSerialIoManager.stop()
    }

    private fun startIoManager() {
        mSerialIoManager = SerialInputOutputManager(mPort, mListener)
        mExecutor.submit(mSerialIoManager)
    }

    override fun connect(context: Context, port: UsbSerialPort): Boolean {
        // create empty files TODO: create log
        var list: List<String> = getUARTList()
        for (file in list)
            FileManager.createEmptyFile(mPath, file)



        try {
            mPort = port
            val usbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
            val connection = usbManager.openDevice(mPort.getDriver().getDevice())
            mPort.open(connection);
            mPort.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE)

            startIoManager()

        } catch (e: IOException) {
            mPort.close()
        }
        return true
    }

    override fun disconnect(): Boolean {
        return true;
    }

    override fun read(): String {
        return "UART READ"
    }

    override fun write(uartName: String, data: String) {
        var timestamp = System.currentTimeMillis()

        var formatter = SimpleDateFormat("hh:mm:ss:SS")
        var dateString = formatter.format(Date(timestamp))


        if (uartName == "BIC"){
            FileManager.writeFile(mPath, CommandList.bic, "["+dateString +"] : "+data + "\n")
        } else {
            FileManager.writeFile(mPath, uartName, "["+dateString +"] : "+data + "\n")
            FileManager.writeFile(mPath, CommandList.bic, "["+dateString +"] : "+data + "\n")
        }

        var s = data + "\n"
        mPort.write(s.toByteArray(), 100)

        mDataListenner.dataNotify(data)
    }

    override fun clear(uartName: String) {
        FileManager.clear(mPath, uartName)
    }

    override fun isConnect(): Boolean {
        return true
    }

    override fun getUARTList(): List<String> {
        return CommandList.getListFile()
    }

    override fun getBaudRateList(): List<String> {
        return arrayListOf("300", "1200", "2400", "4800", "9600", "14400", "19200", "28800", "38400", "57600", "115200", "230400")
    }

    override fun getUARTOutput(uartName: String): String {
        return FileManager.readFile(mPath, uartName)
    }

    override fun connectToUart(uartName: String, baudrate: String) {
    }

    override fun isUartConnect(uartName: String): Boolean {
        return false;
    }

    override fun isDataAvailable(): Boolean {
        return mDataAvailable;
    }

    override fun setDataListenner(l: DataNotify): Boolean {
        mDataListenner = l
        return true
    }
}