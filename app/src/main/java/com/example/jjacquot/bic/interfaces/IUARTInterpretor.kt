package com.example.jjacquot.bic.interfaces

import android.content.Context
import com.hoho.android.usbserial.driver.UsbSerialPort

/**
 * Created by jjacquot on 17/10/17.
 */
interface IUARTInterpretor {

    /**Connect to device return connection status*/
    fun connect(context: Context, port: UsbSerialPort): Boolean

    /**Connect to device return connection status*/
    fun connectToUart(uartName: String, baudrate: String)

    /**Get UART Status*/
    fun isUartConnect(uartName: String): Boolean

    /**Disconnect the device return connection status*/
    fun disconnect(): Boolean

    /**read data*/
    fun read(): String

    /**Write data*/
    fun write(uartName: String, data: String)

    /**Clear output*/
    fun clear(uartName: String)

    /** Notify thats data are avaible*/
    fun isDataAvailable(): Boolean

    /**Return the status of the connection*/
    fun isConnect(): Boolean

    /** Return the list of UART accessible*/
    fun getUARTList(): List<String>

    /** getBaudrates*/
    fun getBaudRateList(): List<String>

    /** Return the output of a particular UART*/
    fun getUARTOutput(uartName: String): String

    fun setDataListenner(l : DataNotify) : Boolean

}

interface DataNotify{
    /** notify user that data are here */
    fun dataNotify(data :String)
}