package com.example.jjacquot.bic

import com.example.jjacquot.bic.interfaces.IBIC

/**
 * Created by jips on 10/18/17.
 */
class MockBIC : IBIC {

    var isC: Boolean = false

    override fun isConnect(): Boolean {
        return isC
    }

    override fun connect() {
        isC = true
    }

    override fun disconnect() {
        isC = false
    }

    override fun read(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun write(data: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}