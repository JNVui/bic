package com.example.jjacquot.bic

import java.io.File

/**
 * Created by jjacquot on 17/10/17.
 */
class FileManager {

    companion object {
        /** Create file */
        fun createFile(path: String, name: String): Boolean {
            var file: File = File(path + name)
            return file.createNewFile()
        }

        /** Create empty file */
        fun createEmptyFile(path: String, name: String): Boolean {
            var file: File = File(path + name)
            if (file.exists()){
                file.writeText("")
                return true
            } else {
                return file.createNewFile()
            }
        }

        /** Read file */
        fun readFile(path: String, name: String): String {
            var file: File = File(path + name)
            return file.readText()
        }

        /** Write in file*/
        fun writeFile(path: String, name: String, data: String) {
            var file: File = File(path + name)
            file.appendText(data)
        }

        /** Delete file */
        fun DeleteFile(path: String, name: String): Boolean {
            var file: File = File(path + name)
            return file.delete()
        }


        /** List File from Directory */
        fun listFileFromDirectory(path: String, name: String): List<String> {
            return arrayListOf("UART1", "UART2", "UART3", "UART4", "CAN1", "CAN2", "I/O s")
        }

        fun clear(path: String, name: String) {
            var file: File = File(path + name)
            file.writeText("")
        }


    }

}